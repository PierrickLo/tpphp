<?php
session_start();
require_once('include/fonction.php');
require_once('include/connexion.php');

//

//Initialisation de la variable $id, qui contiendra soit l'id de la ville soit celui du fournisseur, si rien n'est récupérer, l'id vaut 0

/* Si l'id de la ville est présent dans l'url (= quand on vient de la liste) on utilise celui-ci
Sinon on regarde si il y a un id de la ville qui vient d'être créée pour pouvoir le modifier juste 
après sa création sans avoir à recharger la page de la ville qui amène à la perte du message de création*/

if(isset($_POST['Nouveau'])) {
	unset($_SESSION['createdID']);
}

//$id = (isset($_GET['id']))?$_GET['id']:0;
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	unset($_SESSION['createdID']);
} else if (isset($_SESSION['createdID'])) {
	$id = $_SESSION['createdID'];
} else {
	$id = 0;
}
//Action du bouton 'Supprimer', permet de supprimer une ville
if(isset($_POST['Supprimer']) && $_SESSION['ville'] == 1) {
	$_SESSION['MSG_SUPP'] = '';
	try {
		$requete = $bdd->prepare('DELETE FROM ville WHERE code = :code');
		$requete->bindParam(':code', $_POST['code'], PDO::PARAM_INT);
		$requete->execute();
		header("Location:$url/TPPHP/listeville.php");
	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
	$_SESSION['MSG_SUPP'] = 'Suppression ok !';
	die();
}



/* Condition de retour à la page listefournisseur:
Si l'id vaut 0 ET si la récupération du bouton 'Nouveau' est vite
Ou si on clic sur 'Annuler' */
if(isset($_POST['Annuler']) OR ($id == 0 && empty($_POST['Nouveau']) && empty($_POST['Creer']))) {
	header("Location:$url/TPPHP/listeville.php");
	die();
} // Si on a cliqué sur "Annuler", retour à la liste des villes


if(isset($_POST['Modifier']) OR isset($_POST['Creer'])) {
	//Initialisation de la variable de session KO à vide
	$_SESSION['MSG_KO'] = '';

	// Si le nom comporte moins de 2 caractère Alors MSG_KO
	if(strlen($_POST['nom']) < 2) {
		$_SESSION['MSG_KO'] .= "Le nom doit comporter minimum 2 caractères.<br>" ;
	}
	// Si le code postal ne fait pas 5 caractères et si il est pas de type numérique Alors MSG_KO
	if(strlen($_POST['codepostal']) < 5 OR strlen($_POST['codepostal']) > 5 OR ctype_digit($_POST['codepostal'])==false) {
		$_SESSION['MSG_KO'] .= "Le code postal est obligatoire et doit comporter 5 caractères numérique." ;
	}
	// Si le pays n'est pas correct Alors MSG_KO
	if($_POST['pays'] != "France" AND $_POST['pays'] != "Andorre" AND $_POST['pays'] != "Monaco") {
		$_SESSION['MSG_KO'] .= "Le pays renseigné doit être la France, Andorre ou Monaco (avec la première lettre en majuscule) !<br>";
	}


	/*Condition qui test si la variable de session KO est vide, si elle n'est pas vide je vérifie si une ville est déjà existant pour éviter les doublons*/
	if(empty($_SESSION['MSG_KO'])){

		$requete = $bdd->prepare('SELECT count(*) as cpt FROM ville WHERE LOWER(nom) = LOWER(:nom)');
		$requete->execute(array( 'nom' => $_POST['nom'] ));
		$compteur = $requete->fetch();
		if($compteur['cpt'] > 0) {
			$_SESSION['MSG_KO'] .= "le nom (".$_POST['nom'].") est déjà pris<br />";
		}
	}
	/* Si la variable de session MSG_KO et qu'un clic est effectué sur Modifier, 
	et que l'utilisateur à les droits de modification sur les villes, 
	Alors il peut la mettre à jour*/
	if(isset($_POST['Modifier']) && empty($_SESSION['MSG_KO']) && $_SESSION['ville'] == 1){
		try {
			$requete = $bdd->prepare('UPDATE ville
				SET nom = :nom, 
				codepostal = :codepostal, 
				pays = :pays
				WHERE code = :code');

			$requete->execute(array('nom' => $_POST['nom'],
				'codepostal' => $_POST['codepostal'], 
				'pays' => $_POST['pays'],
				'code' => $_POST['code']
				));
		} catch (PDOException $e) {
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}
		$_SESSION['MSG_OK'] = "Modification bien enregistrée";
	}
	/* Si la variable de session MSG_KO est vide et qu'un clic est effectué sur Créer, 
	Alors l'utilisateur peut créer une ville */
	if(isset($_POST['Creer']) && empty($_SESSION['MSG_KO'])) {
		$_SESSION['MSG_CREE'] = '';
		try {
			$requete = $bdd->prepare('insert into ville (nom, codepostal, pays) values
				(:nom, :codepostal, :pays)');

			$requete->execute(array('nom' => $_POST['nom'],
				'codepostal' => $_POST['codepostal'], 
				'pays' => $_POST['pays'], ));
		} catch (PDOException $e) {
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}
		// on met message de succès
		$_SESSION['MSG_OK'] = "Création bien enregistrée";
		// on récupère l'id du fournisseur créé
		$id = $bdd->lastInsertId();
		$_SESSION['createdID'] = $id;
	}
} 
try {
	$requete = $bdd->prepare('SELECT code, nom, codepostal, pays FROM ville WHERE code = ?');
	$requete->execute(array($id)); //la requete est executée après avoir remplacer le ? par $id
	$ville = $requete->fetch();//lecture du résultat de la requete et chargée dans un tableau
	// Toutes les infos sont dans un tableau $ville
} catch (PDOException $e) {
	print"Erreur!:" . $e->getMessage() . "<br/>";
	die();
}
?>

<!DOCTYPE html>
<html lang="fr">  
<head>    
	<meta charset="utf-8">    
	<meta name="viewport" content="width=device-width, initial-scale=1.0">    
	<title>Ville <?php echo $ville['nom']; ?></title>    
	<link href="css/bootstrap.css" rel="stylesheet">    
	<link href="css/style.css" rel="stylesheet">
	<link media ="screen and (max-width: 900px)" href="css/smartphone.css" rel="stylesheet">  
</head>  
<body> 
	
	<?php
	include('include/menu.php');
	echo afficheMessages();
	?> 

	<div class="container">      
		<h1><?php echo selectCMV($id)?> Ville <?php echo $ville['nom']; ?></h1>
	

	<form method="post" class="col-sm-10">
		<div class="form-group row">			
			<input class="col-sm-10" type="hidden" name="code" value="<?php echo $ville['code'] ?>" >
		</div>
		<div class="form-group row">
			<label class="col-form-label col-sm-2">Nom</label>
			<input class="col-sm-10" type="text" name="nom" value="<?php echo $ville['nom'] ?>" >
		</div>
		<div class="form-group row">
			<label class="col-form-label col-sm-2">Code postal</label>
			<input class="col-sm-10" type="text" name="codepostal" value="<?php echo $ville['codepostal'] ?>" >
		</div>
		<div class="form-group row">
			<label class="col-form-label col-sm-2">Pays</label>
			<input class="col-sm-10" type="text" name="pays" value="<?php echo $ville['pays'] ?>" >
		</div>

		<div class="form-group row float-right ">
			<input type="submit" class="btn btn-default" name="Annuler"
			value="Annuler">
			<?php echo selectInput($id); ?>
		</div>

	</form>
	</div>

	<script src="js/jquery.js"></script>
	<script>
		$(function() {
			$('.confirm').click(function() {
				return window.confirm("Êtes-vous sur ?");
			});
		});
	</script>
</body>
</html>
