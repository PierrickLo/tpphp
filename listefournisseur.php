<?php
session_start();
require_once('include/fonction.php');
?>


<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Liste des fournisseurs</title>
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link media ="screen and (max-width: 900px)" href="css/smartphone.css" rel="stylesheet"> 
</head>
<body>

	<?php
	include('include/menu.php');

	echo afficheMessages();
	?>


	<div class="container" id="Srow">



		<h1>Les fournisseurs </h1>

		<div class="row" id="Srow">
			<table class="table table-hover" id="fournisseurs">
				<thead> <!--Initialisation des noms de mes colonnes -->
					<tr>
						<th>Civilité</th>
						<th>Nom</th>
						<th>Contact</th>
						<th>Code postal</th>
						<th>Ville</th>
						<!--<th>Code fournisseur</th> -->
					</tr>
				</thead>

				<tbody >
					<?php
					include('include/connexion.php');
					//echo afficheMessages();

				// Requete SQL
					$requete = 'SELECT civilite.libelle, fournisseur.nom, fournisseur.contact, ville.codepostal, ville.nom AS ville, fournisseur.code AS code_fournisseur
					FROM fournisseur, civilite, ville
					WHERE civilite.code = fournisseur.civilite AND fournisseur.ville = ville.code'; 
					try {
						foreach($bdd->query($requete) as $ligne) { // Pour chaque fournisseur trouvé, affichage des fournisseurs et de leurs données par lignes, dans les colonnes voulu
							echo '<tr class = "clickable-row" data-href="fournisseur.php?id=' . $ligne['code_fournisseur'] .'">';
							echo '<td>' . $ligne['libelle'] . '</td>';
							echo '<td>' . $ligne['nom'] . '</td>';
							echo '<td>' . $ligne['contact'] . '</td>';
							echo '<td>' . $ligne['codepostal'] . '</td>';
							echo '<td>' . $ligne['ville'] . '</td>';
							//echo '<td>' . $ligne['code_fournisseur'] . '</td>';
							echo "</tr>\n";
						} 
					} catch (PDOException $e) {
						print 'Erreur !: ' . $e->getMessage() . '<br/>';
						die();
					}
					?>
				</tbody>
			</table>

			<?php 	
				if($_SESSION['fournisseur'] == 1) {
				// on ne met le bouton "Nouveau" que si il a le droit
			?>
			<div class="container">
				<form method="post" action="fournisseur.php">
					<div class="form-group row float-right">
						<input type="submit" class="btn btn-primary" name="Nouveau" value="Nouveau"/>
					</div>
				</form>
			</div>
			<?php  
				}
			?>
		</div>

	</div>

<script src="js/jquery.js"></script>    
<script>
$(document).ready(function($) {
	$(".clickable-row").click(function() {
		window.location = $(this).data("href");        
	});      
});    

</script>

</body>
</html>