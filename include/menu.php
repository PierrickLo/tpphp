<?php
require_once('include/fonction.php');
require_once('include/connexion.php');
?>



<?php
global $bdd;

// Gestion de la connxeion
if(isset($_POST['Connexion'])) {
	try {
		$requete = $bdd->prepare('SELECT code, password, ville, fournisseur FROM
			utilisateur WHERE login = lower(?)');
		$requete->execute(array(/*$_SESSION['login'] => */$_POST['login']));
		$utilisateur = $requete->fetch();
	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
	// Si le login est inconnu
	if(empty($_POST['login'])/* OR $_POST['login'] != 'login' */) {
		// On met un message d'erreur générique
		$_SESSION['MSG_KO'] = 'Login invalide ' . $_POST['login'] . $_POST['password'];
	} else {
		// test du mot de passe saisi
		$hash = $_POST['password'];
		if(password_verify($hash, $utilisateur['password'])) {
			$_SESSION['login'] = $_POST['login'];
			$_SESSION['code'] = $utilisateur['code'];
			$_SESSION['ville'] = $utilisateur['ville'];
			$_SESSION['fournisseur'] = $utilisateur['fournisseur'];
		} else {
			// On met le même message d'erreur que pour "login raté"
			$_SESSION['MSG_KO'] = "Mdp invalide";
		}
	}
}
// Gestion de la déconnxeion
if(isset($_POST['Deconnexion'])) {
	unset($_SESSION['login']);
	unset($_SESSION['code']);
	unset($_SESSION['ville']);
	unset($_SESSION['fournisseur']);
	$_SESSION['MSG_OK'] = "Vous êtes bien déconnecté";  
}

?>

		<!-- Initialisation du menu /Index, Fournisseur, Ville/  
		Utilisation de la fonction menuActif pour ajouter "active" à la classe en question-->

		<nav class="nav nav-pills nav-fill" id="Smenu" name="Snav">   
			<a class="nav-item nav-link <?php echo menuActif('index'); ?>" href="index.php"><img src="images/code.png" height="30" width="60" alt="Revenir à l'accueil" name="Slogo">Accueil</a>
			<a class="nav-item nav-link <?php echo menuActif('fournisseur'); ?>" href="http://localhost/TPPHP/listefournisseur.php">Fournisseur</a>
			<a class="nav-item nav-link <?php echo menuActif('ville'); ?>" href="http://localhost/TPPHP/listeville.php">Ville</a>

		</nav>
		<div class="container-fluid"> 
			<div class="row">
				<div class="col align-self-end">

				<divid="Sconnex" style="float: right;padding: 0.3rem 1rem;"> <?php echo formulaireLogin(); ?> </div>
				</div>
			</div>
		</div>




