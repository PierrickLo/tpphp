<?php
/**
 * Fonction qui génère un input type select avec toutes les villes 
 * @param  $id    id du select 
 * @return code HTML à afficher 
 */
function selectVille($id, $code) {
	global $bdd;
	//Initilisation du selec
	$retour = "<select class='form-control' id='$id' name='$id'>";
	
	try {
		//Requete SQL
		$requete = 'SELECT code, nom FROM ville';

		foreach($bdd->query($requete) as $ligne) {
			
			$retour .= '<option value='.$ligne['code'];
			if($ligne['code'] == $code) {
				$retour .= ' selected="selected"' ;
			}
			$retour .= '>' . $ligne['nom'] . '</option>';
		}    
	} catch (PDOException $e) {
		print"Erreur!:" . $e->getMessage() . "<br/>";
		die();
	}
	$retour .= "</select>";
	return $retour;

}

?>

<?php
/**
 * Fonction qui génère un input type select avec toutes les villes 
 * @param  $id    id du select 
 * @return code HTML à afficher 
 */
function selectCivilite($id, $code) {
	global $bdd;
	$retour = "<select class='form-control' id='$id' name='$id'>";

	try {
		$requete = 'SELECT code, libelle FROM civilite';

		foreach($bdd->query($requete) as $ligne) {

			$retour .= '<option value='.$ligne['code'].'';
			if($ligne['code'] == $code) {
				$retour .= ' selected="selected"';
			}
			$retour.= '>' . $ligne['libelle'] . '</option>';
		}    
	} catch (PDOException $e) {
		print"Erreur!:" . $e->getMessage() . "<br/>";
		die();
	}
	$retour .= "</select>";
	return $retour;
}


/**
* Fonction qui génère la classe 'active' si le menu est sélectionné
* @param $menu Nom du menu d'appel
* @return code HTML à afficher
*/
function menuActif($menu) {
	$ecran = basename($_SERVER['SCRIPT_FILENAME'], ".php"); //Récupération du nom de la page active
	$actif =''; // Initialisation de la variable $activ qui contiendra la suite des "class" dans menu.php
	if ($ecran == 'listefournisseur' && $menu == 'fournisseur' OR ($ecran == 'fournisseur' && $menu == 'fournisseur')) {
		$actif .= 'active';// J'ajoute 'active' dans la variable $actif
		return $actif;// Ajout dans la "class" pour donner un aspect unique à la page active si les conditions sont remplies, lorsque je suis sur listefournisseur.php
	}
	if ($ecran == 'listeville' && $menu == 'ville'OR ($ecran == 'ville' && $menu == 'ville')) {
		$actif .= 'active';// J'ajoute 'active' dans la variable $actif
		return $actif;// Ajout dans la "class" pour donner un aspect unique à la page active si les conditions sont remplies, lorsque que je suis sur listeville.php
	}
	if ($ecran == 'index' && $menu == 'index'OR ($ecran == 'index' && $menu == 'index')) {
		$actif .= 'active';// J'ajoute 'active' dans la variable $actif
		return $actif;// Ajout dans la "class" pour donner un aspect unique à la page active si les conditions sont remplies, lorsque que je suis sur listeville.php
	}
}

//Fonction permettant d'afficher des messages 
function afficheMessages() {
	$retour = '';
	if(!empty($_SESSION['MSG_OK'])) {
		$retour .= '<div class="alert alert-success">' . $_SESSION['MSG_OK'] .
		'</div>'."\n";
		unset($_SESSION['MSG_OK']);
	}
	if(!empty($_SESSION['MSG_KO'])) {
		$retour .= '<div class="alert alert-danger">' . $_SESSION['MSG_KO'] .
		'</div>'."\n";
		unset($_SESSION['MSG_KO']);
	}
	if(!empty($_SESSION['MSG_SUPP'])) {
		$retour .= '<div class="alert alert-success">' . $_SESSION['MSG_SUPP'] .
		'</div>'."\n";
		unset($_SESSION['MSG_SUPP']);
	}

	return $retour;
}

//Fonction qui permet d'afficher créer si il l'id est à 0, et supprimer si l'id existe
function selectInput($id) {
	$ecran = basename($_SERVER['SCRIPT_FILENAME'], ".php");
	$input = '';

	if(($ecran == 'fournisseur' && $_SESSION['fournisseur'] == 1) OR ($ecran == 'ville' && $_SESSION['ville'] == 1)) {
		if($id > 0) {
			$input .= '<input type="submit" class="btn btn-danger confirm" id="btn" name="Supprimer" value="Supprimer"> <input type="submit" class="btn btn-primary" name="Modifier" value="Modifier">';
		}
		if($id == 0) {
			$input .= '<input type="submit" class="btn btn-primary" name="Creer" value="Creer">';
		}
	}
	return $input;

}

//Fonction qui affiche "nouveau" lors d'une création de fournisseur
function selectCMF($id) {
	$cm = '';
			
		if(isset($_POST['Nouveau']) OR (isset($_POST['Creer']) && $id == 0)) {
			$cm .= 'Nouveau';
		}

	return $cm;

}
//Fonction qui affiche "nouvelle" lors d'une création de ville
function selectCMV($id) {
	$cm = '';

	if(isset($_POST['Nouveau']) OR (isset($_POST['Creer']) && $id == 0)) {
		$cm .= 'Nouvelle';
	}

	return $cm;

}


//Fonction qui permet d'afficher si l'utilisateur est connecté, et si il n'est pas connecté cela affiche les champs de connexion
function formulaireLogin() {
	$retour ='';
	if(isset($_SESSION['code']) && $_SESSION['code'] <> 0) {// Si on est connecté
		$retour = '
		<form class="form-inline" id="Sconnex" method="post">    
			<div class="form-group" id="Scon">      
				<label>Connecté en tant que : ' . $_SESSION['login'] . '&nbsp;</label>      
				<button type="submit" class="btn btn-primary btn-sm" name="Deconnexion">Se déconnecter</button>      
			</div>    
		</form>';
	} else {
		$retour ='
		<form class="form-inline" id="Sconnex" method="post">
			<div class="">
				<input type="text" class="form-control" id="login" name="login"
					placeholder="Identifiant">
				<input type="password" class="form-control" id="password" name="password"
					placeholder="Mot de passe">
				<button type="submit" class="btn btn-primary btn-sm"  name="Connexion">Se
					connecter</button>
			</div>
		</form>';
	$_SESSION['login'] = 0;
	$_SESSION['code'] = 0;
	$_SESSION['ville'] = 0;
	$_SESSION['fournisseur'] = 0;

	}
		return $retour;
	}


?>