<?php
session_start();
require_once('include/fonction.php');
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Liste des villes</title>
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/datatables.css" rel="stylesheet">
	<link media ="screen and (max-width: 900px)" href="css/smartphone.css" rel="stylesheet">
</head>
<body>
	<?php
	include('include/menu.php');
	echo afficheMessages();
	?>

	<div class="container">

		<h1>Les villes </h1>

		<div class="row" >
			<table class="table table-striped table-hover" id="villes">
				<thead>
					<tr>
						<th>Nom</th>
						<th>Code postal</th>
						<th>Pays</th>
					</tr>
				</thead>
				<tbody>

					<?php
					include('include/connexion.php');
					echo afficheMessages();

					$requete = 'SELECT nom, codepostal, pays, code
					FROM ville'
					;
					try {
					foreach($bdd->query($requete) as $ligne) { // Pour chaque ville trouvé, affichage des villes et des données sélectionnées par lignes, dans les colonnes voulu
						echo '<tr class = "clickable-row" data-href="ville.php?id=' . $ligne['code'] .'">';
						echo '<td>' . $ligne['nom'] . '</td>';
						echo '<td>' . $ligne['codepostal'] . '</td>';
						echo '<td>' . $ligne['pays'] . '</td>';
						echo "</tr>\n";
					}
				} catch (PDOException $e) {
					print 'Erreur !: ' . $e->getMessage() . '<br/>';
					die();
				}
				?>

			</tbody>
		</table>
			<?php
				if($_SESSION['ville'] == 1) {
				// on ne met le bouton "Nouveau" que si il a le droit
			?>
			<div class="container">
				<form method="post" action="ville.php">
					<div class="form-group row float-right">
						<input type="submit" class="btn btn-primary" name="Nouveau" value="Nouveau">
					</div>
				</form>
			</div>
			<?php
				}
			?>
		</div>
	</div>


	<script src="js/jquery.js"></script>
	<script src="js/datatables.js"></script>
	<script>
		$(document).ready(function() {
			$('#villes').DataTable( {
				"language": {
					"url": "js/datatables.french.lang"
				}
			} );
		} );
	</script>



<script>
	$(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});

</script>


</body>
</html>
