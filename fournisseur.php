<?php
session_start();
require_once('include/fonction.php');
require_once('include/connexion.php');

//

//Initialisation de la variable $id, qui contiendra soit l'id de la ville soit celui du fournisseur, si rien n'est récupéré, l'id vaut 0

if(isset($_POST['Nouveau'])) {
	unset($_SESSION['createdID']);
}

//$id = (isset($_GET['id']))?$_GET['id']:0;
/* Si l'id du fournisseur est présent dans l'url (= quand on vient de la liste) on utilise celui-ci
Sinon on regarde si il y a un id du fournisseur qui vient d'être créé pour pouvoir le modifier juste 
après sa création sans avoir à recharger la page du fournisseur qui amène à la perte du message de création*/
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	unset($_SESSION['createdID']);
} else if (isset($_SESSION['createdID'])) {
	$id = $_SESSION['createdID'];
} else {
	$id = 0;
}


//Action du bouton 'Supprimer', permet de supprimer un fournisseur
if(isset($_POST['Supprimer']) && $_SESSION['fournisseur'] == 1) {
	$_SESSION['MSG_SUPP'] = '';
	try {
		$requete = $bdd->prepare('DELETE FROM fournisseur WHERE code = :code');
		$requete->bindParam(':code', $_POST['code'], PDO::PARAM_INT);
		$requete->execute();
	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
	$_SESSION['MSG_SUPP'] = 'Suppression ok !';
	header("Location:$url/TPPHP/listefournisseur.php");
	die();
}


/* Condition de retour à la page listefournisseur:
Si l'id vaut 0 ET si la récupération du bouton 'Nouveau' est vite
Ou si on clic sur 'Annuler' */
if(isset($_POST['Annuler']) OR ($id == 0 && empty($_POST['Nouveau']) && empty($_POST['Creer']))) {
	header("Location:$url/TPPHP/listefournisseur.php");
	die();
 } // Si on a cliqué sur "Annuler", retour à la liste des fournisseurs


//Enchainement de condition afin d'arriver ou nom à la modification d'un fournisseur
 if(isset($_POST['Modifier']) or isset($_POST['Creer'])) {
	$_SESSION['MSG_KO'] = ''; //Initialisation de la variable de session KO à vide
	
	if(strlen($_POST['nom']) < 3) {
		//Si le nom est invérieur à 3 caractères, je rempli la variable de session KO
		$_SESSION['MSG_KO'] .= "Le nom doit comporter minimum 3 caractères.<br>" ;
	}
	if(empty($_POST['adresse1'])) {
		//Si l'adresse est vide je rempli la variable de session KO
		$_SESSION['MSG_KO'] .= "L'adresse est obligatoire." ;
	}


	/*Condition qui test si la variable de session KO est vide, si elle n'est pas vide je vérifie si un fournisseur est déjà existant pour éviter les doublons*/
	if(empty($_SESSION['MSG_KO']) ){

		$requete = $bdd->prepare('SELECT count(*) as cpt FROM fournisseur WHERE LOWER(nom) = LOWER(:nom)');
		$requete->execute(array( 'nom' => $_POST['nom'] ));
		$compteur = $requete->fetch();
		if($compteur['cpt'] > 0) {
			$_SESSION['MSG_KO'] .= "le nom (".$_POST['nom'].") est déjà pris<br />";
		}
	}
	/*Si la variable de session KO est vide et que l'utilisateur à les droits, 
	je procède à la modifition de mon fournisseur et 
	je remplis ma variable de session OK indiquer que la modification 
	a bien été effectuée*/
	if(isset($_POST['Modifier']) && empty($_SESSION['MSG_KO']) && $_SESSION['fournisseur'] == 1){
		try {
			$requete = $bdd->prepare('UPDATE fournisseur
				SET nom = :nom, 
				adresse1 = :adresse1, 
				adresse2 = :adresse2, 
				ville = :ville, 
				contact = :contact, 
				civilite = :civilite
				WHERE code = :code');

			$requete->execute(array('nom' => $_POST['nom'],
				'adresse1' => $_POST['adresse1'], 
				'adresse2' => $_POST['adresse2'], 
				'ville' => $_POST['ville'],
				'contact' => $_POST['contact'],
				'civilite' => $_POST['civilite'],
				'code' => $_POST['code']
				));
		} catch (PDOException $e) {
			print "Erreur !: " . $e->getMessage() . "<br/>";
			die();
		}
		$_SESSION['MSG_OK'] = "Modification bien enregistrée";
	}
/* Si la variable de session MSG_KO est vide et qu'un clic est effectué sur Créer, 
Alors l'utilisateur peut créer un fournisseur */
if(isset($_POST['Creer']) && empty($_SESSION['MSG_KO'])) {
	$_SESSION['MSG_CREE'] = '';
	try {
		$requete = $bdd->prepare('insert into fournisseur (nom, adresse1, adresse2,
			ville, contact, civilite) values
		(:nom, :adresse1, :adresse2, :ville, :contact, :civilite)');

		$requete->execute(array('nom' => $_POST['nom'],
			'adresse1' => $_POST['adresse1'], 
			'adresse2' => $_POST['adresse2'], 
			'ville' => $_POST['ville'],
			'contact' => $_POST['contact'],
			'civilite' => $_POST['civilite']));


	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
		// on met message de succès
	$_SESSION['MSG_OK'] = "Création bien enregistrée";
		// on récupère l'id du fournisseur créé
	$id = $bdd->lastInsertId();
	$_SESSION['createdID'] = $id;


}
} 
try {
	$requete = $bdd->prepare('SELECT code, nom, adresse1, adresse2, ville, contact, civilite FROM fournisseur WHERE code = ?');
	$requete->execute(array($id)); //la requete est executée après avoir remplacé le ? par $id
	$fournisseur = $requete->fetch();//lecture du résultat de la requete et chargée dans un tableau
	// Toutes les infos sont dans un tableau $fournisseur
}	 catch (PDOException $e) {
	print"Erreur!:" . $e->getMessage() . "<br/>";
	die();
}


?>






<!DOCTYPE html>
<html lang="fr">  
<head>    
	<meta charset="utf-8">    
	<meta name="viewport" content="width=device-width, initial-scale=1.0">    
	<title>Fournisseur <?php echo $fournisseur['nom']; ?></title>    
	<link href="css/bootstrap.css" rel="stylesheet">    
	<link href="css/style.css" rel="stylesheet">  
	<link media ="screen and (max-width: 900px)" href="css/smartphone.css" rel="stylesheet">

</head>  
<body>  
	<?php
	include('include/menu.php');
	echo afficheMessages();

	?>  

	<div class="container">      
		<h1> <?php echo selectCMF($id)?> Fournisseur <?php echo $fournisseur['nom']; ?></h1>
		
		<form method="post" class="col-sm-10">
			<div class="form-group row">			
				<input class="col-sm-10 form-control" type="hidden" name="code" value="<?php echo $fournisseur['code'] ?>" >
			</div>
			<div class="form-group row">
				<label class="col-form-label col-sm-2">Nom</label>
				<input class="col-sm-10 form-control" type="text" name="nom" value="<?php echo $fournisseur['nom'] ?>" >
			</div>
			<div class="form-group row">
				<label class="col-form-label col-sm-2">Adresse</label>
				<input class="col-sm-10 form-control" type="text" name="adresse1" value="<?php echo $fournisseur['adresse1'] ?>" >
			</div>
			<div class="form-group row">
				<label class="col-form-label col-sm-2">Suite adresse</label>
				<input class="col-sm-10 form-control" type="text" name="adresse2" value="<?php echo $fournisseur['adresse2'] ?>" >
			</div>
			<div class="form-group row"> 
				<label class="col-form-label col-sm-2" for="ville">Ville</label>          
				<div class="col-sm-10">
					<?php echo selectVille('ville', $fournisseur['ville']); ?>         
				</div>        
			</div>
			<div class="form-group row">
				<label class="col-form-label col-sm-2">Contact</label>
				<input class="col-sm-10 form-control" type="text" name="contact" value="<?php echo $fournisseur['contact'] ?>" >
			</div>
			<div class="form-group row"> 
				<label class="col-form-label col-sm-2" for="civilite">Civilite</label>          
				<div class="col-sm-10">
					<?php echo selectCivilite('civilite', $fournisseur['civilite']); ?>         
				</div>        
			</div>

			<div class="form-group row float-right">
				<input type="submit" class="btn btn-default" name="Annuler"
				value="Annuler">
				<?php echo selectInput($id); ?>

			</div>

		</form>

	</div>

	<script src="js/jquery.js"></script>
	<script>
	$(function() {
		$('.confirm').click(function() {
			return window.confirm("Êtes-vous sur ?");
		});
	});
	</script>

</body>
</html>
